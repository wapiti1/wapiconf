#!/usr/bin/env python
# -*- coding: UTF-8

import click
import time


def gen_title(title: str, clear: bool = True):
    """
    Print a title on the screen.
    Setting clear to false will prevent clearing the console
    when printing the title.
    """
    if clear:
        click.clear()
    title = """
    ####{}####
    #   {}   #
    ####{}####\n\n""".format('#'*len(title), title, '#'*len(title))
    click.secho(title, fg='green')


def gen_prompt(prompt: str, options: list, title: str = "", print_title: bool
               = False, clear: bool = False, sleep: int = 0):
    """
    Prompt the user with a multiple-choice menu. This prompt can be customised
    with a title, whenever the screen will be cleared before displaying the
    title and if a waiting time should be imposed after the user input their
    choice.
    """
    if clear:
        click.clear()
    if print_title:
        gen_title(title, False)
    prompt += '\n'
    for i in range(len(options)):
        prompt += '    '+str(i+1)+' - '+options[i]+'\n'
    while True:
        res = click.prompt(prompt+'\nChoice', default=len(options), type=int)
        if res >= 1 and res <= len(options):
            time.sleep(sleep)
            return res
        else:
            click.clear()
            click.secho('Please enter a correct input!', fg='red')
            time.sleep(3)
            gen_title(title)
