#!/usr/bin/python3.8
# -*- coding: UTF-8

from string import Template
from time import sleep
from os import path
import click
import menu
import yaml
import os

# Global vars for important paths
TEMPLATES_PATH = path.dirname(path.abspath(__file__))+"/templates/"
CONF_PATH = path.dirname(path.dirname(path.abspath(__file__)))+"/promtail.yaml"


##########################################################
#                        Commands                        #
##########################################################


@click.group()
def cli():
    pass


@cli.command(help="Generate a new configuration file")
def gen():
    """
    This command allows the user to generate a new configuration file from the
    baseconf template inside the TEMPLATE_PATH folder. It replace the old file
    from CONF_PATH.
    """
    title = "Gen conf"
    try:
        conf_data = yaml.safe_load(open(TEMPLATES_PATH + "baseconf.yaml", "rt")
                                   .read())
    except FileNotFoundError:
        click.secho("No base configuration file found.\nAborting !", fg="red")
        exit(1)

    try:
        conf_file = open(CONF_PATH, "w")
    except FileNotFoundError or IOError:
        click.secho("Cannot write to: "+CONF_PATH+".\nAborting !", fg="red")
        exit(1)

    options = get_template_list()
    options.append("Quit this program.")

    # Print the menu title then generate the base config (loki url
    # in this case)
    menu.gen_title(title, False)
    gen_replace(conf_data)
    conf_data = conf_data["template"]
    scrapes_modules = []

    # Prompt for using templates until the user is done
    while True:
        res = menu.gen_prompt("Choose a template:", options, title, True)
        if res == len(options):
            break
        res -= 1  # Convert res to an index
        scrapes_modules.append(
            yaml.safe_load(open(TEMPLATES_PATH + options[res] + ".yaml", "rt")
                           .read())
        )
        gen_replace(scrapes_modules[-1])
        scrapes_modules[-1] = scrapes_modules[-1]["template"]

    # Concatenate scrape configs and write to the configuration file
    conf_data["scrape_configs"] = []
    for module in scrapes_modules:
        conf_data["scrape_configs"].append(module[0])
    conf_file.write(yaml.dump(conf_data, width=1000, sort_keys=False))
    exit(0)


@cli.command(help="Manage curret config (Add or remove scrape configs, ...)")
def manage():
    """
    The main loop that allows a user to manage the current configuration in
    CONF_PATH.
    It allows them to add new entries or delete old ones without deleting the
    whole file like the gen command would do.
    """
    title = "Manage conf"

    # Try getting the config in ../config/promtail.yaml. If not present, print
    # "file not found" and quit
    try:
        conf_data = yaml.safe_load(open(CONF_PATH, "rt").read())
    except FileNotFoundError:
        click.secho("No configuration file found.\nAborting !", fg="red")
        exit(1)

    # Manage main loop
    while True:
        cmd = menu.gen_prompt(
            "Add a new field from template or delete one ?",
            ["Add new field", "Delete a field", "Quit this program."],
            title,
            True,
            True,
        )

        # Add new field
        if cmd == 1:
            template_list = get_template_list()
            template_list.append("Back")
            res = menu.gen_prompt("Choose a template:", template_list, title)
            if res != len(template_list):
                new_module = yaml.safe_load(
                    open(TEMPLATES_PATH + template_list[res - 1] + ".yaml",
                         "rt").read()
                )
                gen_replace(new_module)
                new_module = new_module["template"][0]
                while new_module["job_name"] in get_configs(
                    conf_data["scrape_configs"]
                ):
                    new_module["job_name"] += "-new"
                conf_data["scrape_configs"].append(new_module)

        # Delete field
        elif cmd == 2:
            options = get_configs(conf_data["scrape_configs"])
            options.append("Back")
            field = menu.gen_prompt("Choose a field to delete:", options,
                                    title)
            if field != len(options) and click.confirm("Are you sure ?"):
                print("Removed field " + options[field - 1] + ".")
                conf_data["scrape_configs"].pop(field - 1)
                sleep(2)

        # Quit program, saving change
        elif cmd == 3:
            break

    open(CONF_PATH, "wt").write(yaml.dump(conf_data, width=1000,
                                sort_keys=False))
    exit(0)


@cli.command("list", help="List available templates.")
def print_list():
    flist = get_template_list()
    print("Available templates are:")
    for f in flist:
        print("    - " + f)
    exit(0)


###########################################################
#                        Functions                        #
###########################################################


def get_template_list():
    """
    Returns the list of available templates beside baseconf.yaml
    """
    flist = os.listdir(TEMPLATES_PATH)
    res: list = []
    for f in flist:
        if f != "baseconf.yaml":
            res.append(f.replace(".yaml", ""))
    return res


def get_configs(conf: list):
    res = []
    for elem in conf:
        res.append(elem["job_name"])
    return res


def gen_replace(dic: dict):
    """
    Crawl through the config to get to the wanted field then change its
    content with the wanted value.
    """
    for keyword in dic["template_fields"]:
        print("\n" + dic["template_fields"][keyword]["helper"])
        value = click.prompt(
            "Value for " + keyword,
            default=dic["template_fields"][keyword]["default_value"],
        )
        for pos in dic["template_fields"][keyword]["pos"]:
            curr_pos = dic["template"]
            pos_list = pos.split(".")
            for i in range(len(pos_list) - 1):
                if isinstance(curr_pos, dict):
                    curr_pos = curr_pos[pos_list[i]]
                elif isinstance(curr_pos, list):
                    curr_pos = curr_pos[0][pos_list[i]]
            if isinstance(curr_pos, list):
                curr_pos = curr_pos[0]
            curr_pos[pos.split(".")[-1]] = Template(
                curr_pos[pos.split(".")[-1]]
            ).safe_substitute({keyword: value})


if __name__ == "__main__":
    cli()
