
# Python executable
PYTHON_EXECUTABLE=python3

# ------------------------ wapiConf ------------------------ #
.PHONY: run-gen run-manage run-list run init

run-gen: ARG=gen
run-gen: run

run-manage: ARG=manage
run-manage: run

run-list: ARG=list
run-list: run

run:
	@bash -c "cd wapiconf && poetry run ./wapiconf.py ${ARG}"

init:
	@-mkdir -p config
	@bash -c "cd wapiconf && poetry install"